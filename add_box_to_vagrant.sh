#!/bin/bash -eux

vagrant box add ubuntu18.instrument output/ubuntu-18.04.instrument.box
#vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-vbguest --plugin-clean-sources --plugin-source https://rubygems.org

ssh-keygen -t rsa -P ""
/c//temp/id_rsa

vagrant up
